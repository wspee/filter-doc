.. filter-doc documentation master file, created by
   sphinx-quickstart on Thu Jun 28 11:04:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to filter-doc's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/test
   pages/introduction



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
